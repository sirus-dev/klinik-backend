import Sequelize from 'sequelize';
import { functionLogger } from '../lib';
import { db } from '../lib/db';

// Model definition for all required table
// const ModelName = db.define('tableName', {
//     columnName: {type: Sequelize.Type(length), options}
// }, { freezeTableName: boolean, timestamps: boolean});
const UserModel = db.define('users', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    role_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    // karyawan_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    // perawat_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    // dokter_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    username: { type: Sequelize.STRING },
    password: { type: Sequelize.STRING },
    last_login: { type: Sequelize.DATE },
    is_deleted: { type: Sequelize.BOOLEAN },
}, { freezeTableName: true, timestamps: true, underscored: true });

const RoleModel = db.define('role', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    nama: { type: Sequelize.STRING },
    endpoint: { type: Sequelize.JSON },
    is_deleted: { type: Sequelize.BOOLEAN },
}, { freezeTableName: true, timestamps: false });

const LoginHistoryModel = db.define('login_history', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    user_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    login: { type: Sequelize.DATE },
    logout: { type: Sequelize.DATE },
    latitude: { type: Sequelize.DOUBLE },
    longitude: { type: Sequelize.DOUBLE },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: false });

const ProvinceReferenceModel = db.define('ref_propinsi', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    kode_provinsi: { type: Sequelize.STRING },
    nama_provinsi: { type: Sequelize.STRING },
    kode_pos: { type: Sequelize.STRING },
    latitude: { type: Sequelize.STRING },
    longitude: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: false });

const CityReferenceModel = db.define('ref_kota', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    kode_provinsi: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    nama_kota: { type: Sequelize.STRING },
    kode_pos: { type: Sequelize.STRING },
    latitude: { type: Sequelize.STRING },
    longitude: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: false });

const KecamatanReferenceModel = db.define('ref_kecamatan', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    kode_provinsi: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    kode_kecamatan: { type: Sequelize.STRING },
    nama_kecamatan: { type: Sequelize.STRING },
    kode_pos: { type: Sequelize.STRING },
    latitude: { type: Sequelize.STRING },
    longitude: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: false });

const KelurahanReferenceModel = db.define('ref_kelurahan', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    kode_provinsi: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    kode_kecamatan: { type: Sequelize.STRING },
    kode_kelurahan: { type: Sequelize.STRING },
    nama_kelurahan: { type: Sequelize.STRING },
    kode_pos: { type: Sequelize.STRING },
    latitude: { type: Sequelize.STRING },
    longitude: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: false });

const KlinikTypeModel = db.define('klinik_type', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    nama: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: false });

const HospitalTypeModel = db.define('rumah_sakit_type', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    nama: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: false });

const ClassificationTypeModel = db.define('klasifikasi', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    nama: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: false });

const RegistrationModel = db.define('pendaftaran', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    klinik_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    pasien_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    klasifikasi_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    poliklinik_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    dokter_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    perawat_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    status_kunjungan: { type: Sequelize.STRING },
    no_pendaftaran: { type: Sequelize.STRING },
    tgl_kunjungan: { type: Sequelize.DATE },
    umur: { type: Sequelize.INTEGER },
    berat_badan: { type: Sequelize.INTEGER },
    tinggi_badan: { type: Sequelize.INTEGER },
    tekanan_darah: { type: Sequelize.STRING },
    napas: { type: Sequelize.INTEGER },
    nadi: { type: Sequelize.INTEGER },
    suhu: { type: Sequelize.INTEGER },
    keterangan: { type: Sequelize.STRING },
    finish_vital: { type: Sequelize.BOOLEAN },
    finish_periksa: { type: Sequelize.BOOLEAN },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: false });

const KlinikModel = db.define('klinik', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    klinik_type_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    nama: { type: Sequelize.STRING },
    alamat: { type: Sequelize.STRING },
    kode_kelurahan: { type: Sequelize.STRING },
    kode_kecamatan: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    kode_provinsi: { type: Sequelize.STRING },
    no_telp: { type: Sequelize.STRING },
    no_fax: { type: Sequelize.STRING },
    no_hp: { type: Sequelize.STRING },
    email: { type: Sequelize.STRING },
    sik: { type: Sequelize.STRING },
    sip: { type: Sequelize.STRING },
    tgl_reg: { type: Sequelize.STRING },
    penanggung_jawab: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_active: { type: Sequelize.BOOLEAN },
    is_deleted: { type: Sequelize.BOOLEAN },
}, { freezeTableName: true, timestamps: true, underscored: true });

const HospitalModel = db.define('rumah_sakit', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    rumah_sakit_jenis_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false},
    nama: { type: Sequelize.STRING },
    alamat: { type: Sequelize.STRING },
    kode_kelurahan: { type: Sequelize.STRING },
    kode_kecamatan: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    kode_provinsi: { type: Sequelize.STRING },
    no_telp: { type: Sequelize.STRING },
    no_fax: { type: Sequelize.STRING },
    no_hp: { type: Sequelize.STRING },
    email: { type: Sequelize.STRING },
    sik: { type: Sequelize.STRING },
    sip: { type: Sequelize.STRING },
    tgl_reg: { type: Sequelize.STRING },
    penanggung_jawab: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: true, underscored: true });

const CorporationModel = db.define('perusahaan', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    nama: { type: Sequelize.STRING },
    alamat: { type: Sequelize.STRING },
    kode_kelurahan: { type: Sequelize.STRING },
    kode_kecamatan: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    kode_provinsi: { type: Sequelize.STRING },
    no_telp: { type: Sequelize.STRING },
    no_fax: { type: Sequelize.STRING },
    no_hp: { type: Sequelize.STRING },
    email: { type: Sequelize.STRING },
    sk_reg: { type: Sequelize.STRING },
    tgl_reg: { type: Sequelize.STRING },
    penanggung_jawab: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: true, underscored: true});

const PolyClinicModel = db.define('poliklinik', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    nama: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_active: { type: Sequelize.BOOLEAN },
    is_deleted: { type: Sequelize.BOOLEAN },
    created_by: { type: Sequelize.INTEGER },
    updated_by: { type: Sequelize.INTEGER }
}, { freezeTableName: true, timestamps: true, underscored: true });

const DokterModel = db.define('dokter', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    klinik_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    poliklinik_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    no_ktp: { type: Sequelize.STRING },
    no_npwp: { type: Sequelize.STRING },
    nama: { type: Sequelize.STRING },
    tmpt_lahir: { type: Sequelize.STRING },
    tgl_lahir: { type: Sequelize.STRING },
    jenis_kelamin: { type: Sequelize.STRING },
    gol_darah: { type: Sequelize.STRING },
    alamat: { type: Sequelize.STRING },
    kode_kelurahan: { type: Sequelize.STRING },
    kode_kecamatan: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    kode_provinsi: { type: Sequelize.STRING },
    agama: { type: Sequelize.STRING },
    pekerjaan: { type: Sequelize.STRING },
    perkawinan: { type: Sequelize.STRING },
    no_telp: { type: Sequelize.STRING },
    no_hp: { type: Sequelize.STRING },
    email: { type: Sequelize.STRING },
    sip: { type: Sequelize.STRING },
    s1: { type: Sequelize.STRING },
    s1_tahun: { type: Sequelize.STRING },
    s2: { type: Sequelize.STRING },
    s2_tahun: { type: Sequelize.STRING },
    s3: { type: Sequelize.STRING },
    s3_tahun: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: true, underscored: true });

const NurseModel = db.define('perawat', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    klinik_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    no_ktp: { type: Sequelize.STRING },
    no_npwp: { type: Sequelize.STRING },
    nama: { type: Sequelize.STRING },
    tmpt_lahir: { type: Sequelize.STRING },
    tgl_lahir: { type: Sequelize.STRING },
    jenis_kelamin: { type: Sequelize.STRING },
    gol_darah: { type: Sequelize.STRING },
    alamat: { type: Sequelize.STRING },
    kode_kelurahan: { type: Sequelize.STRING },
    kode_kecamatan: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    kode_provinsi: { type: Sequelize.STRING },
    agama: { type: Sequelize.STRING },
    pekerjaan: { type: Sequelize.STRING },
    perkawinan: { type: Sequelize.STRING },
    no_telp: { type: Sequelize.STRING },
    no_hp: { type: Sequelize.STRING },
    email: { type: Sequelize.STRING },
    sip: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: true, underscored: true });

const EmployeeModel = db.define('karyawan', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    klinik_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false},
    no_ktp: { type: Sequelize.STRING },
    no_npwp: { type: Sequelize.STRING },
    nama: { type: Sequelize.STRING },
    tmpt_lahir: { type: Sequelize.STRING },
    tgl_lahir: { type: Sequelize.DATE },
    jenis_kelamin: { type: Sequelize.STRING },
    gol_darah: { type: Sequelize.STRING },
    alamat: { type: Sequelize.STRING },
    kode_kelurahan: { type: Sequelize.STRING },
    kode_kecamatan: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    kode_provinsi: { type: Sequelize.STRING },
    agama: { type: Sequelize.STRING },
    pekerjaan: { type: Sequelize.STRING },
    perkawinan: { type: Sequelize.STRING },
    no_telp: { type: Sequelize.STRING },
    no_hp: { type: Sequelize.STRING },
    email: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: true, underscored: true });

const PatientModel = db.define('pasien', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    perusahaan: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    no_ktp: { type: Sequelize.STRING },
    no_bpjs: { type: Sequelize.STRING },
    no_rm: { type: Sequelize.STRING },
    nama: { type: Sequelize.STRING },
    tmpt_lahir: { type: Sequelize.STRING },
    tgl_lahir: { type: Sequelize.STRING },
    jenis_kelamin: { type: Sequelize.STRING },
    gol_darah: { type: Sequelize.STRING },
    alamat: { type: Sequelize.STRING },
    kode_kelurahan: { type: Sequelize.STRING },
    kode_kecamatan: { type: Sequelize.STRING },
    kode_kota: { type: Sequelize.STRING },
    kode_provinsi: { type: Sequelize.STRING },
    agama: { type: Sequelize.STRING },
    perkawinan: { type: Sequelize.STRING },
    pekerjaan: { type: Sequelize.STRING },
    no_telp: { type: Sequelize.STRING },
    no_hp: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: true, underscored: true });

const MedrecModel = db.define('rekam_medis', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    klinik_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    pasien_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    klasifikasi_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    poliklinik_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    dokter_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    perawat_id: { type: Sequelize.INTEGER, foreignKey: true, NULL: false },
    no_pendaftaran: { type: Sequelize.STRING },
    tgl_kunjungan: { type: Sequelize.DATE },
    umur: { type: Sequelize.INTEGER },
    berat_badan: { type: Sequelize.INTEGER },
    tinggi_badan: { type: Sequelize.INTEGER },
    tekanan_darah: { type: Sequelize.STRING },
    napas: { type: Sequelize.INTEGER },
    nadi: { type: Sequelize.INTEGER },
    suhu: { type: Sequelize.INTEGER },
    status_selesai: { type: Sequelize.BOOLEAN },
    anamnesa: { type: Sequelize.TEXT },
    diagnosa: { type: Sequelize.TEXT },
    terapi: { type: Sequelize.TEXT },
    tindakan: { type: Sequelize.TEXT },
    keterangan: { type: Sequelize.STRING },
    is_finished: { type: Sequelize.BOOLEAN },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: true, underscored: true });

const RecipeCodeModel = db.define('resep_kode', {
    id: { type: Sequelize.INTEGER, primaryKey: true, NULL: false },
    kode: { type: Sequelize.STRING },
    deskripsi: { type: Sequelize.STRING },
    keterangan: { type: Sequelize.STRING },
    is_deleted: { type: Sequelize.BOOLEAN }
}, { freezeTableName: true, timestamps: true, underscored: true });

// // Model Relation
// Model1.belongsTo(Model2, {foreignKey: 'columnName'});
UserModel.belongsTo(RoleModel);

// Export defined models
const Users = db.models.users;
const Role = db.models.role;
export {
    Users,
    Role
};

// /**
//  * Function to get last id from intended table
//  * @param modelName Model name where id needed
//  * @param sortingRow Intended row in selected model
//  */
// export async function getLastID(modelName: any, sortingRow: any) {
//     functionLogger.debug(`Model: ${modelName}, Row: ${sortingRow}`);

//     // search all data in selected table and sort it by selected row
//     let result = await modelName.findOne({ order: [[sortingRow, 'DESC']], limit: 1, raw: true });

//     // if no result/table is empty, return 0 as current id
//     if ( result == null ) {
//         result = {
//             [sortingRow]: '0'
//         };
//     }

//     // return current id
//     functionLogger.debug('Current Id: ' + result[sortingRow]);
//     return result[sortingRow];
// }