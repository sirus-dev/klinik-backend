import * as rc from 'rc';
import * as process from 'process';
import { join, dirname, resolve } from 'path';
import { existsSync, readFileSync, readFile } from 'fs';
import { LogLevel } from 'bunyan';

/**
 * Interface for config
 */
export interface Config {
    APP_PORT: number;
    SEQUELIZE: {
        HOST: string;
        PORT: number;
        DB: string;
        USERNAME: string;
        PASSWORD: string;
        DIALECT: string;
        LOGGING: boolean;
    };
    LOG_LEVEL: LogLevel;
    LOCALHOST: string;
    SECRET: string;
    CONSID: string;
    CONSSECRET: string;
    BASEURL: string;
}

// Setting default config in case .json config doesn't exist
export const configDefault: Config = {
    APP_PORT: 5000,
    SEQUELIZE: {
        HOST: '104.248.151.31',
        PORT: 14031,
        DB: 'klinik',
        USERNAME: 'root',
        PASSWORD: 'sukses2018',
        DIALECT: 'postgres',
        LOGGING: false
    },
    LOG_LEVEL: 'fatal',
    LOCALHOST: 'localhost',
    SECRET: 'rahasia',
    CONSID: '6865',
    CONSSECRET: '9oADB26325',
    BASEURL: 'https://dvlp.bpjs-kesehatan.go.id/vclaim-rest'
};

/**
 * Read configuration for binary packaged build
 * configuration must be placed in JSON format with 'sirus.config.json' filename
 * File must be placed in the same directory with distributed binary file ('sirus-api')
 */
function readLocalConfig(): Config {
    const configPath = join(dirname(process.execPath), 'sirus.config.json');
    try {
        if (!existsSync(configPath)) {
            return configDefault;
        }
        const raw = readFileSync(configPath, 'utf8');
        const configLocal = JSON.parse(raw);
        return Object.assign<any, Config, Config>({}, configDefault, configLocal);
    } catch (e) {
        return configDefault;
    }
}

const config: Config = process['pkg'] ?
    readLocalConfig() :
    rc<Config>('sirusapi', configDefault);

export default config;