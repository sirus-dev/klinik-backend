import { authenticate } from '../lib/token';

module.exports = function (app: any, router: any) {
    router.get('/', async function (req: any, res: any) {
        console.log('Method: ' + req.method + ', Path: ' + req.baseUrl);

        res.json({
            status: 'Success',
            message: 'This is Klinik Endpoint'
        });
    });

    app.use('/klinik', authenticate, router);
};