import * as express from 'express';
import * as bodyParser from 'body-parser';
import { createServer } from 'http';
import { graphql, execute, subscribe } from 'graphql';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { PubSub } from 'graphql-subscriptions';
import { makeExecutableSchema } from 'graphql-tools';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import * as cors from 'cors';
import * as jwt from 'jsonwebtoken';
import * as ejs from 'ejs';
import * as path from 'path';
import * as joinMonster from 'join-monster';
import * as joinMonsterAdapt from 'join-monster-graphql-tools-adapter';
// import { typeDefs } from './api/typeMerger';
// import { resolvers } from './api/resolverMerger';
import { authenticate } from './lib';
import config from './config';
const { APP_PORT, LOCALHOST, SECRET } = config;
var router = express.Router();

// Creating Express and defining Public Subscription
const graphQLServer = express();
const pubSub = new PubSub();

// Create Schema from existing Types and Resolvers
// const schema = makeExecutableSchema({
//     typeDefs,
//     resolvers,
// });

/**
 * Define server to use cors
 * Set server endpoint "authGraphiql" to generate token for further access
 * Set server endpoint "graphiql" that required proper token to be accessed
 */
graphQLServer.use(cors());
graphQLServer.use(express.static(path.join(__dirname, 'assets')));
graphQLServer.set('view engine', 'ejs');
graphQLServer.use(bodyParser.urlencoded({ extended: false }));
graphQLServer.use(bodyParser.json());
// graphQLServer.use('/graphql', bodyParser.json(), verify, graphqlExpress({ context: { pubSub, }, schema: schema }));
graphQLServer.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql', subscriptionsEndpoint: `ws://${LOCALHOST}:${APP_PORT}/subscriptions`}));

// Set the routing
graphQLServer.use('/status', function (req: any, res: any) {
    res.render(__dirname + `/views/landing.ejs`);
});

graphQLServer.use('/', function (req: any, res: any) {
    res.json({ message: 'Hello, Welcome to SIRUS Klinik API' });
});
// Defining Server Routing
require('./routes/auth')(graphQLServer, router);
require('./routes/klinik')(graphQLServer, router);

// Creating Server
const APIServer = createServer(graphQLServer);

// Creating server listener
APIServer.listen(APP_PORT, () => {
    console.log(`App is running on http://${LOCALHOST}:${APP_PORT}`);

    // Creating GraphQL Subscription Server using WebSocket
    /*
    const subscriptionServer = SubscriptionServer.create(
        {
            schema,
            execute,
            subscribe,
        },
        {
            server: APIServer,
            path: '/subscriptions',
        },
    );
    */
});