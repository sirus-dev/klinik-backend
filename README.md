# Sirus-Backend-Api-Klinik
---

API for Sirus Klinik

## Requirement

* Node.js: `^10.15.0`
* NPM: `^6.4.1`

## Installation

* After cloning the project into your local workspace, use `npm install` to install all dependencies
* Build the project before starting the API by using `npm run build`
* Run the API on your local workspace by `npm start`
* Go to `localhost:98765/status` or [Click Here](http://localhost:98765/status) to check if the service has been liftoff

## Development & Debugging Run

* use `npm run debug` to build the entire project and run it on debugging mode